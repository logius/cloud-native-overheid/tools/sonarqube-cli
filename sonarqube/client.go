package sonarqube

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

// Http Methods
const (
	MethodGet    = "GET"
	MethodPost   = "POST"
	MethodPut    = "PUT"
	MethodDelete = "DELETE"
)

// Content types
const (
	jsonContentType = "application/json"
)

// Client struct with api methods and accesstoken
type Client struct {
	FQDN     string
	Username string
	Password string
}

func (sonarqubeClient Client) DoGetRequest(requestPath string, response interface{}) error {
	return sonarqubeClient.doJSONRequest(MethodGet, requestPath, nil, response, 200)
}

func (sonarqubeClient Client) DoDeleteRequest(requestPath string) error {
	return sonarqubeClient.doJSONRequest(MethodDelete, requestPath, nil, nil, 200)
}

func (sonarqubeClient Client) DoPutRequest(requestPath string, body interface{}) error {
	return sonarqubeClient.doJSONRequest(MethodPut, requestPath, body, nil, 200)
}

func (sonarqubeClient Client) DoPostRequest(requestPath string, body interface{}, response interface{}) error {
	return sonarqubeClient.doJSONRequest(MethodPost, requestPath, body, response, 200, 204)
}

func (sonarqubeClient Client) doJSONRequest(method string, requestPath string, body interface{}, response interface{}, allowedStatuscode ...int) error {

	buf := new(bytes.Buffer)
	json.NewEncoder(buf).Encode(body)

	url := fmt.Sprintf("%s/api/%s", sonarqubeClient.FQDN, requestPath)

	req, err := http.NewRequest(method, url, buf)
	if err != nil {
		return fmt.Errorf("error while creating a new http request for \"%s\": %v", url, err)
	}

	req.SetBasicAuth(sonarqubeClient.Username, sonarqubeClient.Password)
	req.Header.Set("Content-Type", jsonContentType)

	var client = &http.Client{}
	res, err := client.Do(req)
	if err != nil {
		return fmt.Errorf("error while performing http request: %v", err)
	}
	defer res.Body.Close()

	if !statusCodeOK(res.StatusCode, allowedStatuscode) {
		log.Println("unexpected status code - method ", method, " ", requestPath, "Statuscode ", res.StatusCode)
		responseData, _ := ioutil.ReadAll(res.Body)
		return fmt.Errorf("URL: %s, Method %s,  Error status code: %d responseData: %v", url, method, res.StatusCode, string(responseData))
	}

	responseData, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return fmt.Errorf("unable to perform action readAll: %v", err)
	}

	//fmt.Printf("URL: %s, Method %s, Body: %s, Response: %s\n", url, method, body, responseData)

	if len(responseData) > 0 && response != nil {
		err = json.Unmarshal(responseData, response)
		if err != nil {
			return fmt.Errorf("URL: %s, Method %s, Body: %s, Response: %s, error: %s", url, method, body, responseData, err)
		}
	}

	return nil
}

func (sonarqubeClient Client) ShowVersion() error {

	url := fmt.Sprintf("%s/api/server/version", sonarqubeClient.FQDN)

	res, err := http.Get(url)
	if err != nil {
		return fmt.Errorf("error while creating a new http get request for \"%s\": %v", url, err)
	}

	defer res.Body.Close()

	responseData, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return fmt.Errorf("unable to perform action readAll: %v", err)
	}

	if res.StatusCode != 200 {
		return fmt.Errorf("URL: %s, Error status code: %d responseData: %v", url, res.StatusCode, string(responseData))
	}

	log.Printf("SonarQube version: %v", string(responseData))

	return nil
}

func statusCodeOK(statusCode int, allowedStatuscodes []int) bool {
	for _, code := range allowedStatuscodes {
		if code == statusCode {
			return true
		}
	}
	return false
}
