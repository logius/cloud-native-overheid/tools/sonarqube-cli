package config

import (
	"io/ioutil"

	"github.com/pkg/errors"
	"gopkg.in/yaml.v3"
)

// Rolebinding defines the binding between rol and zero or more groups
type Rolebinding struct {
	Name   string
	Groups []string `yaml:"groups,omitempty"`
}

// sonarqube configuration
type SonarQube struct {
	Rolebindings []Rolebinding `yaml:"roles"`
}

type Customer struct {
	Name      string
	SonarQube SonarQube
}

type Config struct {
	Customer Customer
}

// LoadConfigfile loads the customer configuration from file
func LoadConfigfile(configFile string) (*Customer, error) {
	yamlFile, err := ioutil.ReadFile(configFile)
	if err != nil {
		return nil, errors.Errorf("Error reading YAML file: %s\n", err)
	}
	return NewConfig(string(yamlFile))
}

// NewConfig gets the customer configuration
func NewConfig(configData string) (*Customer, error) {

	var cfg Config
	err := yaml.Unmarshal([]byte(configData), &cfg)
	if err != nil {
		return nil, errors.Errorf("Could not parse the config err = %v", err)
	}

	return &cfg.Customer, nil
}
