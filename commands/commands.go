package commands

import (
	"github.com/spf13/cobra"
	"gitlab.com/logius/cloud-native-overheid/tools/sonarqube-cli/customer"
)

// AddMainCommand adds the main command for the current module
func AddMainCommand(rootCmd *cobra.Command) {
	cmd := &cobra.Command{
		Use:   "sonarqube",
		Short: "OPS tools for sonarqube",
	}
	rootCmd.AddCommand(cmd)

	AddSubCommands(cmd)

}

// AddSubCommands adds subcommands
func AddSubCommands(cmd *cobra.Command) {
	cmd.AddCommand(customer.NewCommand())
}
