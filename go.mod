module gitlab.com/logius/cloud-native-overheid/tools/sonarqube-cli

go 1.17

require (
	github.com/pkg/errors v0.9.1
	github.com/spf13/cobra v1.2.1
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b
)

require (
	github.com/inconshreveable/mousetrap v1.0.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
)
