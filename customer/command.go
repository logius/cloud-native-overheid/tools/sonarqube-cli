package customer

import (
	"fmt"
	"log"
	"net/url"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/logius/cloud-native-overheid/tools/sonarqube-cli/config"
	"gitlab.com/logius/cloud-native-overheid/tools/sonarqube-cli/sonarqube"
)

type flags struct {
	config            *string
	sonarqubeURL      *string
	sonarqubeUser     *string
	sonarqubePassword *string
}

var systemAdminPermissions = []string{"admin"}
var analystPermissions = []string{"scan", "provisioning"}
var qualityManagerPermissions = []string{"profileadmin", "gateadmin"}

// NewCommand creates a new command
func NewCommand() *cobra.Command {

	flags := flags{}

	cmd := &cobra.Command{
		RunE: func(cmd *cobra.Command, args []string) error {
			err := configureRBAC(cmd, &flags)
			if err != nil {
				return err
			}
			return nil
		},
		Use:   "configure-customer",
		Short: "Configure RBAC for a customer in SonarQube",
		Long:  "This command configures RBAC for a customer in SonarQube.",
	}

	flags.config = cmd.Flags().String("config", "", "Path to customer configfile")
	flags.sonarqubeURL = cmd.Flags().String("sonarqube-url", "", "URL of SonarQube")
	flags.sonarqubeUser = cmd.Flags().String("sonarqube-user", "", "SonarQube username (env: SONARQUBE_USERNAME)")
	flags.sonarqubePassword = cmd.Flags().String("sonarqube-password", "", "SonarQube password (env: SONARQUBE_PASSWORD)")

	cmd.MarkFlagRequired("config")
	cmd.MarkFlagRequired("sonarqube-url")

	return cmd
}

func configureRBAC(cmd *cobra.Command, flags *flags) error {

	sonarqubeClient := &sonarqube.Client{
		Username: os.Getenv("SONARQUBE_USERNAME"),
		Password: os.Getenv("SONARQUBE_PASSWORD"),
		FQDN:     *flags.sonarqubeURL,
	}

	if err := sonarqubeClient.ShowVersion(); err != nil {
		return err
	}

	customerConfig, err := config.LoadConfigfile(*flags.config)
	if err != nil {
		return err
	}

	log.Printf("Configure SonarQube RBAC for customer %q", customerConfig.Name)

	userGroupPermissions, err := getUserGroupPermissions(customerConfig)

	if err != nil {
		return err
	}

	if err := configureRolebindings(sonarqubeClient, customerConfig, userGroupPermissions); err != nil {
		return err
	}

	if err := configurePermissions(sonarqubeClient, customerConfig, userGroupPermissions); err != nil {
		return err
	}

	return nil
}

func configureRolebindings(client *sonarqube.Client, customerConfig *config.Customer, userGroupPermissions map[string][]string) error {
	for userGroupName, _ := range userGroupPermissions {

		type UserGroup struct {
			Name        string
			Description string
		}
		type UserGroupResponse struct {
			Groups []UserGroup
		}
		var userGroups UserGroupResponse
		if err := client.DoGetRequest("user_groups/search?q="+userGroupName, &userGroups); err != nil {
			return err
		}

		description := customerConfig.Name + " role"
		if len(userGroups.Groups) == 0 {
			log.Printf("Create usergroup %q for customer %q", userGroupName, customerConfig.Name)
			url := "user_groups/create?name=" + userGroupName + "&description=" + url.QueryEscape(description)
			if err := client.DoPostRequest(url, nil, nil); err != nil {
				return err
			}
		} else {
			if userGroups.Groups[0].Description != description {
				log.Printf("Update usergroup %q for customer %q", userGroupName, customerConfig.Name)
				url := "user_groups/update?currentName=" + userGroupName + "&description=" + url.QueryEscape(description)
				if err := client.DoPostRequest(url, nil, nil); err != nil {
					return err
				}
			} else {
				log.Printf("Usergroup %q for customer %q already exists", userGroupName, customerConfig.Name)
			}
		}
	}

	return nil
}

func configurePermissions(client *sonarqube.Client, customerConfig *config.Customer, userGroupPermissions map[string][]string) error {

	for userGroupName, permissions := range userGroupPermissions {
		for _, permission := range permissions {

			log.Printf("Configure permission %q for usergroup %q", permission, userGroupName)
			url := "permissions/add_group?groupName=" + userGroupName + "&permission=" + permission
			if err := client.DoPostRequest(url, nil, nil); err != nil {
				return err
			}
		}

		// remove permissions not granted anymore
		// there is no API call to get current permissions, so we have to remove each permission not granted, just to make sure
		allSonarPermissions := append(append(systemAdminPermissions, analystPermissions...), qualityManagerPermissions...)
		for _, sonarPermission := range allSonarPermissions {
			if !isGranted(permissions, sonarPermission) {
				log.Printf("Remove permission %q from usergroup %q", sonarPermission, userGroupName)
				url := "permissions/remove_group?groupName=" + userGroupName + "&permission=" + sonarPermission
				if err := client.DoPostRequest(url, nil, nil); err != nil {
					return err
				}
			}
		}

	}
	return nil
}

func getUserGroupPermissions(customerConfig *config.Customer) (map[string][]string, error) {
	userGroupPermissions := make(map[string][]string)
	for _, rolebinding := range customerConfig.SonarQube.Rolebindings {
		if getRolePermissions(rolebinding.Name) == nil {
			return nil, fmt.Errorf("the role %q does not exist in SonarQube", rolebinding.Name)
		}
		for _, userGroupName := range rolebinding.Groups {
			permissions := userGroupPermissions[userGroupName]
			if permissions == nil {
				permissions = make([]string, 0)
			}
			userGroupPermissions[userGroupName] = append(permissions, getRolePermissions(rolebinding.Name)...)
		}
	}
	return userGroupPermissions, nil
}

func getRolePermissions(roleName string) []string {
	switch roleName {
	case "systemadmin":
		return systemAdminPermissions
	case "analyst":
		return analystPermissions
	case "quality-manager":
		return qualityManagerPermissions
	}
	return nil
}

func isGranted(customerPermissions []string, sonarPermission string) bool {
	for _, customerPermission := range customerPermissions {
		if customerPermission == sonarPermission {
			return true
		}
	}
	return false
}
